package chans.emran.chansfulloptions;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;


import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;


import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;

import java.util.Random;

import static android.graphics.Typeface.BOLD;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CheckBox mLeafCB, mHeartCB, mDiamondCB, mCloverCB;
    private ImageView mLeafIM, mHeartIM, mDiamondIM, mCloverIM;
    private HashMap<String,Integer> mLeafImageMap, mCloverImageMap, mHeartImageMap, mDiamondImageMap;
    private Button mainButton, mPeriodAll, mPeriodHalfYear, mPeriodOneYear,mPeriodTwoYear ,mPeriodFiveYear;
    private LinearLayout mCardsWrapper;
    private TextView result;
    private  TextView topTextView;
    private List<CheckBox> mCheckboxList = new ArrayList<>();
    private List<Button> mPeriodButtonList = new ArrayList<>();
    private int selectedPeriodViewId;



    private CheckBox boxan ;
    private CheckBox boxbn ;
    private CheckBox boxcn ;
    private CheckBox boxdn ;

    private TextView anTextTop;
    private TextView bnTextTop;
    private TextView cnTextTop;
    private TextView dnTextTop;
    private Thread myAdsThread;

    private myThreadAds myads;

    private  boolean isBoxChecked = false;

    public List<String> mLines = new ArrayList<>();

    String[] combination;
    Integer randomLine;
    Integer myadthreadtop = 0;

    private int numberOfLines = 4095;
    public static final String mPath = "chans_sort.txt";
    private ArrayList<Integer> checkedBoxesIndex = new ArrayList<>();
    private Boolean runTh = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        StartAppSDK.init(this, "209311911", false);
        loadDataFromFile();
        initImagesMap();
        initViews();

        selectedPeriodViewId = R.id.period_all;
        mainButton.callOnClick();


        myThread my = new myThread();
        new Thread(my).start();

//        myads = new myThreadAds();
//        myAdsThread = new Thread(myads);
//        myAdsThread.start();
        myadthreadtop = 1;

    }

    private void initImagesMap(){
        if(mLeafImageMap == null){
            mLeafImageMap = new HashMap<>();
        }
        for (int i = 7; i <= 14; i++) {
            String key = "leaf" + i;
            int id = getResources().getIdentifier(key, "drawable", getPackageName());
            mLeafImageMap.put(key,id);
        }
        if(mHeartImageMap == null){
            mHeartImageMap = new HashMap<>();
        }
        for (int i = 7; i <= 14; i++) {
            String key = "heart" + i;
            int id = getResources().getIdentifier(key, "drawable", getPackageName());
            mHeartImageMap.put(key,id);
        }
        if(mDiamondImageMap == null){
            mDiamondImageMap = new HashMap<>();
        }
        for (int i = 7; i <= 14; i++) {
            String key = "diamond" + i;
            int id = getResources().getIdentifier(key, "drawable", getPackageName());
            mDiamondImageMap.put(key,id);
        }
        if(mCloverImageMap == null){
            mCloverImageMap = new HashMap<>();
        }
        for (int i = 7; i <= 14; i++) {
            String key = "clover" + i;
            int id = getResources().getIdentifier(key, "drawable", getPackageName());
            mCloverImageMap.put(key,id);
        }
    }


    private int getRandomCardOfType(String type){
        Random random = new Random();
        int r = random.nextInt(8)+7;
        int resId = 0;
        switch (type){
            case "leaf":
                resId = mLeafImageMap.get(type+r);
                break;
            case "diamond":
                resId = mDiamondImageMap.get(type+r);
                break;
            case "heart":
                resId = mHeartImageMap.get(type+r);
                break;
            case "clover":
                resId = mCloverImageMap.get(type+r);
                break;
        }
        return resId;
    }

    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private void shuffleCards(final int selectedPeriodResId){
        mainButton.setEnabled(false);
        final int leafIdImage = getRandomCardOfType("leaf");
        final int heartIdImage = getRandomCardOfType("heart");
        final int diamondIdImage = getRandomCardOfType("diamond");
        final int cloverIdImage = getRandomCardOfType("clover");

        mCardsWrapper.animate().alpha(0).setDuration(200).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLeafIM.setImageResource(leafIdImage);
                mHeartIM.setImageResource(heartIdImage);
                mDiamondIM.setImageResource(diamondIdImage);
                mCloverIM.setImageResource(cloverIdImage);
                mCardsWrapper.animate().alpha(1.0f).setDuration(200).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mainButton.setEnabled(true);
                        setTextResult(selectedPeriodResId);
                    }
                }).start();
            }
        }).start();

    }

    @Override
    public void onBackPressed() {
        StartAppAd.onBackPressed(this);
        super.onBackPressed();
    }


    @Override
    public void onStart(){
        super.onStart();
//        if (myads == null || myAdsThread == null || !myAdsThread.isAlive() || myAdsThread.isInterrupted()){
//            myads = new myThreadAds();
//            myAdsThread = new Thread(myads);
//            myAdsThread.start();
//        }
//        runTh = true;
//        myadthreadtop = 1;
    }

    @Override
    public void onStop(){
        super.onStop();
        runTh = false;
        myadthreadtop = 0;

    }

    @Override
    public void onResume(){
        super.onResume();
        runTh = true;
        myadthreadtop = 1;

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.mainButton:
                shuffleCards(selectedPeriodViewId);
                break;
            case R.id.leaf_check_box:
                setChoiceVisible(mLeafIM,mLeafCB.isChecked());
                break;
            case R.id.clover_check_box:
                setChoiceVisible(mCloverIM,mCloverCB.isChecked());
                break;
            case R.id.heart_check_box:
                setChoiceVisible(mHeartIM,mHeartCB.isChecked());
                break;
            case R.id.diamond_check_box:
                setChoiceVisible(mDiamondIM,mDiamondCB.isChecked());
                break;
            case R.id.period_all:
            case R.id.period_half_year:
            case R.id.period_1year:
            case R.id.period_2years:
            case R.id.period_5years:
                setSelectedPeriodButton(view);

        }
    }

    private boolean disableLastChoiceFromUnselected() {
        int choicesNumber = getNumberUncheckedChoices();
        if(choicesNumber >= 3){
            for(CheckBox checkBox : mCheckboxList){
                checkBox.setEnabled(!checkBox.isChecked());
            }
        }else{
            for(CheckBox checkBox : mCheckboxList){
                checkBox.setEnabled(true);
            }
        }
        return choicesNumber > 3;
    }

    private void clearPeriodButtons(){
        mPeriodHalfYear.setBackgroundResource(R.drawable.time_period_shape_unselected);
        mPeriodOneYear.setBackgroundResource(R.drawable.time_period_shape_unselected);
        mPeriodTwoYear.setBackgroundResource(R.drawable.time_period_shape_unselected);
        mPeriodFiveYear.setBackgroundResource(R.drawable.time_period_shape_unselected);
        mPeriodAll.setBackgroundResource(R.drawable.time_period_shape_unselected);
    }

    private void setSelectedPeriodButton(View button){
        selectedPeriodViewId = button.getId();
        clearPeriodButtons();
        button.setBackgroundResource(R.drawable.time_period_shape_selected);
        setTextResult(button.getId());
    }

    private void setChoiceVisible(final ImageView imageView, boolean visible){
        if(disableLastChoiceFromUnselected()){
            return;
        }
        if(visible){
            imageView.setVisibility(View.VISIBLE);
            imageView.setAlpha(0.0f);
            imageView.animate().alpha(1.0f).setDuration(200).setListener(null).start();
        }else{
           imageView.animate().alpha(0f).setDuration(200).setListener(new AnimatorListenerAdapter() {
               @Override
               public void onAnimationEnd(Animator animation) {
                   imageView.setVisibility(View.INVISIBLE);
               }
           });
        }
    }

    private int getNumberUncheckedChoices() {
        int counter = 0;
        for(CheckBox checkBox : mCheckboxList){
            if(!checkBox.isChecked()){
                counter++;
            }
        }
        return counter;
    }

    class myThreadAds implements Runnable {
        public void run() {
            while (true) {
                while(runTh){
                    try {
                        Random ran=new Random();

                        int sleep_time=ran.nextInt(70000);

                        if(sleep_time<48000 &&  sleep_time > 38000)
                            Thread.sleep(sleep_time);
                        else  Thread.sleep(35000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (runTh) {
                                StartAppAd.showAd(getApplicationContext());
                            }
                        }

                    });
                }

            }
        }

    }


    class myThread implements Runnable {
        public void run() {
            int x = 0;
            while (true) {
                while (myadthreadtop == 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            topTextView.scrollBy(-10, 0);
                        }
                    });
                    try {
                        Thread.sleep(87);
                        x = (x + 7);
                        int wid = 55;
                        LinearLayout fram = (LinearLayout) findViewById(R.id.mainfr);

                        if (fram != null)
                            wid = fram.getWidth();/**/
                        if (x >= wid) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int wid = 55;
                                    LinearLayout fram = (LinearLayout) findViewById(R.id.mainfr);

                                    if (fram != null)
                                        wid = fram.getWidth();
                                    topTextView.scrollTo(wid, 0);
                                }
                            });
                            x = 1;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }





    public void boxCheked(View view) {

        Log.d("boxCheked","boxCheked");

        int boxId = view.getId();

        if (boxId == boxan.getId()) {

            isBoxChecked = false;
            if (checkedBoxesIndex.get(1) == 1){
                anTextTop.setVisibility(View.INVISIBLE);
                boxan.setChecked(false);
                checkedBoxesIndex.set(1,0);

            }else {
                anTextTop.setVisibility(View.VISIBLE);
                isBoxChecked = true;

                boxan.setChecked(true);
                checkedBoxesIndex.set(1,1);

            }

        }

        if (boxId == boxbn.getId()) {
            isBoxChecked = false;

            if (checkedBoxesIndex.get(2) == 1){
                bnTextTop.setVisibility(View.INVISIBLE);
                boxbn.setChecked(false);
                checkedBoxesIndex.set(2,0);

            }else {
                bnTextTop.setVisibility(View.VISIBLE);
                isBoxChecked = true;

                boxbn.setChecked(true);
                checkedBoxesIndex.set(2,1);

            }

        }

        if (boxId == boxcn.getId()) {
            isBoxChecked = false;

            if (checkedBoxesIndex.get(3) == 1){
                cnTextTop.setVisibility(View.INVISIBLE);
                boxcn.setChecked(false);
                checkedBoxesIndex.set(3,0);
            }else {
                cnTextTop.setVisibility(View.VISIBLE);
                isBoxChecked = true;
                boxcn.setChecked(true);
                checkedBoxesIndex.set(3,1);

            }

        }

        if (boxId == boxdn.getId()) {
            isBoxChecked = false;
            if (checkedBoxesIndex.get(4) == 1){
                dnTextTop.setVisibility(View.INVISIBLE);
                boxdn.setChecked(false);
                checkedBoxesIndex.set(4,0);

            }else {
                dnTextTop.setVisibility(View.VISIBLE);
                boxdn.setChecked(true);
                checkedBoxesIndex.set(4,1);
                isBoxChecked = true;
            }
        }
       // mainButton(view);
    }


    public void mainButton(View view) {

        Integer numberOfMatchedLines = 0;
        Random rand = new Random();
        if (view.getId()  == findViewById(R.id.mainButton).getId() || isBoxChecked ){
            randomLine = rand.nextInt(numberOfLines);
            combination = mLines.get(randomLine).split(" ");
        }
        int anCount = 0;
        int bnCount = 0;
        int cnCount = 0;
        int dnCount = 0;
        int i = 0;
        while (i < numberOfLines){

            boolean isMath = true;

            if (checkedBoxesIndex.get(1) != 0){
                int ranMax = new Random().nextInt(5)+5;
                if (!(mLines.get(i).split(" ")[1].equals(combination[1]))){
                    isMath = false;
                }else if (i < randomLine + ranMax && i > randomLine-ranMax  ){
                    anCount += Integer.valueOf(mLines.get(i).split(" ")[0]);
                }
                anTextTop.setText(combination[1]);
            }else {
                combination[1] = "";
            }

            if (checkedBoxesIndex.get(2) != 0){
                int ranMax = new Random().nextInt(5)+5;

                if (!(mLines.get(i).split(" ")[2].equals(combination[2]))){
                    isMath = false;
                }else if (i < randomLine + ranMax && i > randomLine-ranMax  ){
                    bnCount += Integer.valueOf(mLines.get(i).split(" ")[0]);

                }
                bnTextTop.setText(combination[2]);
            }else {
                combination[2] = "";
            }

            if (checkedBoxesIndex.get(3) != 0){
                int ranMax = new Random().nextInt(70)+5;

                if (!(mLines.get(i).split(" ")[3].equals(combination[3]))){
                    isMath = false;
                }else if (i < randomLine + ranMax && i > randomLine- ranMax ){
                    cnCount += Integer.valueOf(mLines.get(i).split(" ")[0]);
                }
                cnTextTop.setText(combination[3]);
            }else {
                combination[3] = "";

            }

            if (checkedBoxesIndex.get(4) != 0){
                int ranMax = new Random().nextInt(70)+5;
                if (!(mLines.get(i).split(" ")[4].equals(combination[4]))){
                    isMath = false;
                }else if (i < randomLine + ranMax && i > randomLine-ranMax  ){
                    dnCount += Integer.valueOf(mLines.get(i).split(" ")[0]);
                }
                dnTextTop.setText(combination[4]);
            }else {
                combination[4] = "";
            }
            if (isMath){
                numberOfMatchedLines += Integer.valueOf(mLines.get(i).split(" ")[0]);
            }
            i++;
        }


        if (checkedBoxesIndex.get(1) != 0 && checkedBoxesIndex.get(2) != 0 &&
                checkedBoxesIndex.get(3) != 0 && checkedBoxesIndex.get(4) != 0 ){
            result.setText("הצירוף שהוגרל זכה ב "+String.valueOf(mLines.get(randomLine).split(" ")[0]) + " הגרלות ");


        }else {
            result.setText(" הצירוף שהוגרל זכה ב "+String.valueOf(numberOfMatchedLines) + " הגרלות ");
        }
    }


    public void initViews(){
        mCardsWrapper = findViewById(R.id.cards_wrapper);
        mLeafIM = findViewById(R.id.leaf_image);
        mDiamondIM = findViewById(R.id.diamond_image);
        mHeartIM = findViewById(R.id.heart_image);
        mCloverIM = findViewById(R.id.clover_image);

        mLeafCB = findViewById(R.id.leaf_check_box);
        mDiamondCB = findViewById(R.id.diamond_check_box);
        mHeartCB = findViewById(R.id.heart_check_box);
        mCloverCB = findViewById(R.id.clover_check_box);

        mPeriodAll = findViewById(R.id.period_all);
        mPeriodHalfYear = findViewById(R.id.period_half_year);
        mPeriodOneYear = findViewById(R.id.period_1year);
        mPeriodTwoYear = findViewById(R.id.period_2years);
        mPeriodFiveYear = findViewById(R.id.period_5years);
        topTextView = findViewById(R.id.topText);
        mainButton = findViewById(R.id.mainButton);

        mPeriodAll.setOnClickListener(this);
        mPeriodHalfYear.setOnClickListener(this);
        mPeriodOneYear.setOnClickListener(this);
        mPeriodTwoYear.setOnClickListener(this);
        mPeriodFiveYear.setOnClickListener(this);

        mLeafCB.setOnClickListener(this);
        mHeartCB.setOnClickListener(this);
        mDiamondCB.setOnClickListener(this);
        mCloverCB.setOnClickListener(this);
        mainButton.setOnClickListener(this);

        mPeriodButtonList.clear();
        mPeriodButtonList.add(mPeriodAll);
        mPeriodButtonList.add(mPeriodFiveYear);
        mPeriodButtonList.add(mPeriodTwoYear);
        mPeriodButtonList.add(mPeriodOneYear);
        mPeriodButtonList.add(mPeriodHalfYear);

        mCheckboxList.clear();
        mCheckboxList.add(mLeafCB);
        mCheckboxList.add(mHeartCB);
        mCheckboxList.add(mDiamondCB);
        mCheckboxList.add(mCloverCB);

//        boxan = (CheckBox)findViewById(R.id.box1);
//        boxbn = (CheckBox)findViewById(R.id.box2);
//        boxcn = (CheckBox)findViewById(R.id.box3);
//        boxdn = (CheckBox)findViewById(R.id.box4);
//
//        anTextTop = findViewById(R.id.text1);
//        bnTextTop = findViewById(R.id.textView2);
//        cnTextTop = findViewById(R.id.textView3);
//        dnTextTop = findViewById(R.id.textView4);

        result = findViewById(R.id.result);

        checkedBoxesIndex.add(1); // dumy
        checkedBoxesIndex.add(1);
        checkedBoxesIndex.add(1);
        checkedBoxesIndex.add(1);
        checkedBoxesIndex.add(1);

        Random rand = new Random();
        randomLine = rand.nextInt(numberOfLines);
        combination = mLines.get(randomLine).split(" ");
    }

    private void setTextResult(int selectedViewId){
        int wins = 10; //number of winners results -> right now its a dummy number. should be taken from pais databases
        String winsStr = String.valueOf(wins);
        String text = "הצירוף זכה ב "+winsStr+" הגרלות ב";
        switch (selectedViewId){
            case R.id.period_all:
                text = text.concat("סה״כ");
                break;
            case R.id.period_half_year:
                text = text.concat("חצי שנה באחרונה");
                break;
            case R.id.period_1year:
                text = text.concat("שנה האחרונה");
                break;
            case R.id.period_2years:
                text = text.concat("שנתיים האחרונות");
                break;
            case R.id.period_5years:
                text = text.concat("חמש שנים האחרונות");
                break;

        }
        Spannable spannable = new SpannableString(text);
        int start = text.indexOf(winsStr);
        spannable.setSpan(new StyleSpan(BOLD),start,start + winsStr.length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.chans_green)),start,start + winsStr.length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setText(spannable);
    }

    public void loadDataFromFile(){
        AssetManager am = this.getAssets();
        try {
            InputStream is = am.open(mPath);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            int i = 0;
            while (i < numberOfLines) {
                line = reader.readLine();
                mLines.add(line);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}